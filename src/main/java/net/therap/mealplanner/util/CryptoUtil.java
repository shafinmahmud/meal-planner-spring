package net.therap.mealplanner.util;

import org.mindrot.jbcrypt.BCrypt;

/**
 * @author shafin
 * @since 12/12/16
 */
public class CryptoUtil {

    public static String generateSecureHash(String originalString) {
        return BCrypt.hashpw(originalString, BCrypt.gensalt(12));
    }

    public static boolean matchWithSecureHash(String providedString, String existingHash) {
        return BCrypt.checkpw(providedString, existingHash);
    }
}
