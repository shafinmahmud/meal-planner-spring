package net.therap.mealplanner.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author shafin
 * @since 11/14/16
 */
public class RegexUtil {

    public static String getAnyMatched(String text, String regex, int group) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            return matcher.group(group);
        }

        return null;
    }
}
