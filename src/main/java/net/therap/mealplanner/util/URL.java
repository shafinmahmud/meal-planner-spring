package net.therap.mealplanner.util;

/**
 * @author shafin
 * @since 12/12/16
 */
public final class URL {

    public static final String HOME = "/";
    public static final String SIGN_UP = "/signup";
    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/logout";
    public static final String RESOURCES_PREFIX = "/resources";

    public static final String ITEM = "/item";
    public static final String ITEM_INSERT = "/item/insert";
    public static final String ITEM_DELETE = "/item/delete";
    public static final String ITEM_UPDATE = "/item/update";

    public static final String MEAL_INSERT = "/meal/insert";
    public static final String MEAL_UPDATE = "/meal/update";
    public static final String MEAL_DELETE = "/meal/delete";
}
