package net.therap.mealplanner.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author shafin
 * @since 11/13/16
 */
@Entity
@Table(name = "meal")
public class Meal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Day day;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id", nullable = false)
    private MealType type;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "meal_item",
            joinColumns = @JoinColumn(name = "meal_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "item_id", nullable = false))
    private Set<Item> items = new HashSet<>();

    public Meal() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public MealType getType() {
        return type;
    }

    public void setType(MealType type) {
        this.type = type;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public static Day resolveDayType(String dayString) {
        dayString = dayString.toUpperCase().substring(0, 3);

        switch (dayString) {
            case "SAT":
                return Day.SAT;
            case "SUN":
                return Day.SUN;
            case "MON":
                return Day.MON;
            case "TUE":
                return Day.TUE;
            case "WED":
                return Day.WED;
            case "THU":
                return Day.THU;
            case "FRI":
                return Day.FRI;
            default:
                return Day.SUN;
        }
    }

    public static String resolveDayType(Day day) {

        switch (day) {
            case SAT:
                return "Saturday";
            case SUN:
                return "Sunday";
            case MON:
                return "Monday";
            case TUE:
                return "Tuesday";
            case WED:
                return "Wednesday";
            case THU:
                return "Thursday";
            case FRI:
                return "Friday";
            default:
                return "Sunday";
        }
    }
}
