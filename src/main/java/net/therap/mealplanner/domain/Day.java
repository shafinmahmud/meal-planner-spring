package net.therap.mealplanner.domain;

/**
 * @author shafin
 * @since 7/3/18
 */
public enum Day {

    SAT("Saturday"),
    SUN("Sunday"),
    MON("Monday"),
    TUE("Tuesday"),
    WED("Wednesday"),
    THU("Thursday"),
    FRI("Friday");

    private String name;

    Day(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
