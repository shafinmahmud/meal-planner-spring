package net.therap.mealplanner.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
@Entity
@Table(name = "item")
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "items")
    private Collection<Meal> meals = new ArrayList<>();

    public Item() {
    }

    public Item(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Meal> getMeals() {
        return meals;
    }

    public void setMeals(Collection<Meal> meals) {
        this.meals = meals;
    }

    public static List<String> ItemlistToStringList(Collection<Item> list) {
        List<String> stringList = new ArrayList<>();

        for (Item aList: list) {
            stringList.add(aList.getName());
        }

        return stringList;
    }
}
