package net.therap.mealplanner.web.helper;

import net.therap.mealplanner.service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;

/**
 * @author shafin
 * @since 12/15/16
 */
@Component
public class CookieManager {

    public static final String AUTH_COOKIE_NAME = "AUTHENTICATION_VALUE";
    public static final int COOKIE_AGE = 24 * 60 * 60;

    @Autowired
    private AuthorizationService authorizationService;

    public boolean doesContainValidCookie(Cookie[] cookies) {
        if (cookies == null || cookies.length == 0) {
            return false;
        }

        for (Cookie cookie: cookies) {
            if (cookie.getName().equals(AUTH_COOKIE_NAME)) {
                return authorizationService.isAuthTokenValid(cookie.getValue());
            }
        }

        return false;
    }

    public String getAuthCookieValue(Cookie[] cookies) {
        for (Cookie cookie: cookies) {
            if (cookie.getName().equals(AUTH_COOKIE_NAME)) {
                return cookie.getValue();
            }
        }

        return null;
    }

    public Cookie setAuthCookie(String authValue) {
        Cookie cookie = new Cookie(AUTH_COOKIE_NAME, authValue);
        cookie.setMaxAge(COOKIE_AGE);
        return cookie;
    }

    public Cookie invalidateAuthCookie(Cookie[] cookies) {
        for (Cookie cookie: cookies) {
            if (cookie.getName().equals(AUTH_COOKIE_NAME)) {
                authorizationService.removeAuthCookie(cookie.getValue());

                cookie.setMaxAge(0);
                cookie.setValue(null);
                return cookie;
            }
        }

        return null;
    }
}
