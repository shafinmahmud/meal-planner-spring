package net.therap.mealplanner.web.helper;

import net.therap.mealplanner.web.command.MealInfo;
import org.springframework.ui.Model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shafin
 * @since 12/14/16
 */
public class MealHelper {

    public static Model bindMealFormData(List<String> items, Model model, MealInfo mealForm) {

        Map<String, String> dayMap = new LinkedHashMap<>();
        dayMap.put("Saturday", "Saturday");
        dayMap.put("Sunday", "Sunday");
        dayMap.put("Monday", "Monday");
        dayMap.put("Tuesday", "Tuesday");
        dayMap.put("Wednesday", "Wednesday");
        dayMap.put("Thursday", "Thursday");

        Map<String, String> mealTypeMap = new LinkedHashMap<>();
        mealTypeMap.put("Breakfast", "Breakfast");
        mealTypeMap.put("Lunch", "Lunch");
        mealTypeMap.put("Dinner", "Dinner");

        model.addAttribute("availableDays", dayMap);
        model.addAttribute("availableTypes", mealTypeMap);
        model.addAttribute("availableItems", items);
        model.addAttribute("mealForm", mealForm);
        return model;
    }
}
