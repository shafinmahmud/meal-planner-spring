package net.therap.mealplanner.web.filter;

import net.therap.mealplanner.util.URL;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shafin
 * @since 12/11/16
 */
@WebFilter(value = "/*")
public class AuthFilter implements Filter {

    public static final String AUTH_ATTRIBUTE = "auth";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String path = httpRequest.getServletPath();

        httpResponse.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        httpResponse.setHeader("Pragma", "no-cache");

        String sessionAuth = (String) httpRequest.getSession().getAttribute(AUTH_ATTRIBUTE);

        if (isPathInvalid(sessionAuth, path)) {
            httpResponse.sendRedirect(httpRequest.getContextPath() + URL.HOME);
            return;
        }

        if (isExcludedFromFilter(path)) {
            chain.doFilter(request, response);

        } else {

            if (sessionAuth != null) {
                chain.doFilter(request, response);
            } else {
                httpResponse.sendRedirect(httpRequest.getContextPath() + URL.LOGIN);
            }
        }
    }

    public boolean isPathInvalid(String sessionAuth, String path) {
        return sessionAuth != null &&
                (path.startsWith(URL.LOGIN)
                        || path.startsWith(URL.SIGN_UP));
    }

    private boolean isExcludedFromFilter(String path) {
        return path.startsWith(URL.LOGIN)
                || path.startsWith(URL.LOGOUT)
                || path.startsWith(URL.SIGN_UP)
                || path.startsWith(URL.RESOURCES_PREFIX);
    }

    @Override
    public void destroy() {
    }
}
