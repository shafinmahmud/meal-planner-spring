package net.therap.mealplanner.web.validator;

import net.therap.mealplanner.service.AuthorizationService;
import net.therap.mealplanner.service.EmployeeService;
import net.therap.mealplanner.service.LoginInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author shafin
 * @since 12/14/16
 */
@Component
public class UniqueFieldValidator implements ConstraintValidator<UniqueField, Object> {

    @Autowired
    private LoginInfoService loginInfoService;

    @Autowired
    private EmployeeService employeeService;

    private String fieldName;

    @Override
    public void initialize(UniqueField constraintAnnotation) {
        fieldName = constraintAnnotation.fieldName();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        switch (fieldName) {
            case "userName":
                return !loginInfoService.isExistsUserName((String) value);
            case "email":
                return !employeeService.isExistsUserEmail((String) value);
            default:
                return false;
        }
    }
}
