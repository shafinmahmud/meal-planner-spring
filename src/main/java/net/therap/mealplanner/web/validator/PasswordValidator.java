package net.therap.mealplanner.web.validator;

import net.therap.mealplanner.web.command.SignUpForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author shafin
 * @since 12/18/2016
 */
@Component
public class PasswordValidator implements Validator {

    @Override
    public boolean supports(Class c) {
        return SignUpForm.class.isAssignableFrom(c);
    }

    @Override
    public void validate(Object command, Errors errors) {

        SignUpForm form = (SignUpForm) command;
        if (!form.getPassword().equals(form.getVerifyPassword())) {
            errors.rejectValue("verifyPassword", "password.notMatched");
        }
    }
}
