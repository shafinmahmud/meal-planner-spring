package net.therap.mealplanner.web.controller;

import net.therap.mealplanner.service.EmployeeService;
import net.therap.mealplanner.util.URL;
import net.therap.mealplanner.web.command.SignUpForm;
import net.therap.mealplanner.web.validator.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shafin
 * @since 12/8/16
 */
@Controller
public class SignupController {

    private static final String SIGN_UP_VIEW = "signup";

    @Autowired
    private EmployeeService employeeService;

    private PasswordValidator passwordValidator;

    @Autowired
    public void setValidator(PasswordValidator validator) {
        this.passwordValidator = validator;
    }

    @GetMapping(value = URL.SIGN_UP)
    public String showForm(Model model) {
        bindSignUpFormData(model, new SignUpForm());
        return SIGN_UP_VIEW;
    }

    @PostMapping(value = URL.SIGN_UP)
    public String submitForm(@Valid SignUpForm signUpForm, BindingResult result, Model model) {
        passwordValidator.validate(signUpForm, result);
        if (!result.hasErrors()) {
            employeeService.signupEmployee(signUpForm);
            return "redirect:" + URL.LOGIN;
        }

        bindSignUpFormData(model, signUpForm);
        return SIGN_UP_VIEW;
    }

    private static Model bindSignUpFormData(Model model, SignUpForm form) {
        Map<String, String> departmentMap = new HashMap<>();
        departmentMap.put("Software Quality Assurance", "Software Quality Assurance");
        departmentMap.put("Software Developer", "Software Developer");
        departmentMap.put("Database Administrator", "Database Administrator");
        departmentMap.put("System Administrator", "System Administrator");

        form.setPassword("");
        form.setVerifyPassword("");
        model.addAttribute("signUpForm", form);
        model.addAttribute("departments", departmentMap);
        return model;
    }
}