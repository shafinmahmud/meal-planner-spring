package net.therap.mealplanner.web.controller;

import net.therap.mealplanner.service.ItemService;
import net.therap.mealplanner.util.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author shafin
 * @since 12/11/16
 */
@Controller
public class ItemController {

    private static final String ITEM_VIEW = "item";
    private static final String ITEM_EDIT_VIEW = "editItem";

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = URL.ITEM, method = RequestMethod.GET)
    public String showItemList(Model model) {

        model.addAttribute("items", itemService.getItems());
        return ITEM_VIEW;
    }

    @RequestMapping(value = URL.ITEM_INSERT, method = RequestMethod.GET)
    public String showItemForm(Model model) {

        model.addAttribute("formAction", "insert");
        return ITEM_EDIT_VIEW;
    }

    @RequestMapping(value = URL.ITEM_INSERT, method = RequestMethod.POST)
    public String insertItem(@RequestParam("name") String itemName) {

        itemService.insertItem(itemName);
        return "redirect:" + URL.ITEM;
    }

    @RequestMapping(value = URL.ITEM_UPDATE, method = RequestMethod.GET)
    public String showItemForm(@RequestParam("id") long itemId,
                               Model model) {

        model.addAttribute("id", itemId);
        model.addAttribute("name", itemService.findItemName(itemId));
        model.addAttribute("formAction", "update");
        return ITEM_EDIT_VIEW;
    }

    @RequestMapping(value = URL.ITEM_UPDATE, method = RequestMethod.POST)
    public String updateItem(@RequestParam("id") long itemId,
                             @RequestParam("name") String itemName) {

        itemService.updateItem(itemId, itemName);
        return "redirect:" + URL.ITEM;
    }

    @RequestMapping(value = URL.ITEM_DELETE, method = RequestMethod.POST)
    public String deleteItem(@RequestParam(value = "id") int id) {

        itemService.deleteItem(id);
        return "redirect:" + URL.ITEM;
    }
}
