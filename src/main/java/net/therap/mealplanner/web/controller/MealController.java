package net.therap.mealplanner.web.controller;

import net.therap.mealplanner.service.ItemService;
import net.therap.mealplanner.service.MealService;
import net.therap.mealplanner.util.URL;
import net.therap.mealplanner.web.command.MealInfo;
import net.therap.mealplanner.web.helper.MealHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * @author shafin
 * @since 12/11/16
 */
@Controller
public class MealController {

    private static final String MEAL_VIEW = "meal";
    private static final String MEAL_EDIT_VIEW = "editMeal";

    @Autowired
    private MealService mealService;

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = URL.HOME, method = RequestMethod.GET)
    public String meal(Model model) {

        model.addAttribute("meals", mealService.getAllMealInfo());
        return MEAL_VIEW;
    }

    @RequestMapping(value = URL.MEAL_INSERT, method = RequestMethod.GET)
    public String showInsertMeal(Model model) {

        MealHelper.bindMealFormData(itemService.getItemNames(), model, new MealInfo());
        model.addAttribute("formAction", "insert");
        return MEAL_EDIT_VIEW;
    }

    @RequestMapping(value = URL.MEAL_INSERT, method = RequestMethod.POST)
    public String insertMeal(@Valid MealInfo mealForm,
                             BindingResult result,
                             Model model) {

        if (!result.hasErrors()) {
            mealService.updateMeal(mealForm);
            return "redirect:" + URL.HOME;
        }

        MealHelper.bindMealFormData(itemService.getItemNames(), model, mealForm);
        model.addAttribute("formAction", "insert");
        return MEAL_EDIT_VIEW;
    }

    @RequestMapping(value = URL.MEAL_UPDATE, method = RequestMethod.GET)
    public String showUpdateMeal(@RequestParam(value = "id") int id,
                                 Model model) {

        MealInfo mealInfo = mealService.getMealInfo(id);
        MealHelper.bindMealFormData(itemService.getItemNames(), model, mealInfo);
        model.addAttribute("formAction", "update");
        return MEAL_EDIT_VIEW;
    }

    @RequestMapping(value = URL.MEAL_UPDATE, method = RequestMethod.POST)
    public String updateMeal(@Valid MealInfo mealForm,
                             BindingResult result,
                             Model model) {

        if (!result.hasErrors()) {
            mealService.updateMeal(mealForm);
            return "redirect:" + URL.HOME;
        }

        MealHelper.bindMealFormData(itemService.getItemNames(), model, mealForm);
        model.addAttribute("formAction", "update");
        return MEAL_EDIT_VIEW;
    }

    @RequestMapping(value = URL.MEAL_DELETE, method = RequestMethod.POST)
    public String deleteMeal(@RequestParam(value = "id") int id) {

        mealService.deleteMeal(id);
        return "redirect:" + URL.HOME;
    }
}
