package net.therap.mealplanner.web.controller;

import net.therap.mealplanner.service.AuthorizationService;
import net.therap.mealplanner.util.URL;
import net.therap.mealplanner.web.command.LoginForm;
import net.therap.mealplanner.web.filter.AuthFilter;
import net.therap.mealplanner.web.helper.CookieManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * @author shafin
 * @since 7/3/18
 */
@Controller
public class LoginController {

    private static final String LOGIN_VIEW = "login";

    @Autowired
    private AuthorizationService authorizationService;

    @Autowired
    private CookieManager cookieManager;

    @InitBinder("loginForm")
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping(value = URL.LOGIN)
    public String showLogin(HttpServletRequest request, Model model) {

        if (cookieManager.doesContainValidCookie(request.getCookies())) {
            putAuthValueInSession(request);
            return "redirect:" + URL.HOME;
        }

        model.addAttribute("loginForm", new LoginForm());

        return LOGIN_VIEW;
    }

    @PostMapping(value = URL.LOGIN)
    public String doLogin(@Valid LoginForm loginForm, BindingResult result,
                          HttpServletRequest request, HttpServletResponse response, Model model) {

        if (result.hasErrors()) {
            return LOGIN_VIEW;
        }

        if (!authorizationService.isAuthorized(loginForm)) {
            model.addAttribute("errorMessage", "Invalid User Name or Password!");
            return LOGIN_VIEW;
        }

        if (loginForm.isRememberMe()) {
            String authValue = authorizationService.insertUserAuth(loginForm);
            Cookie cookie = cookieManager.setAuthCookie(authValue);
            response.addCookie(cookie);
        }

        putAuthValueInSession(request);
        return "redirect:" + URL.HOME;
    }


    @PostMapping(value = URL.LOGOUT)
    public String doLoginOut(HttpServletRequest request, HttpServletResponse response) {
        Cookie resetCookie = cookieManager.invalidateAuthCookie(request.getCookies());
        if (resetCookie != null) {
            response.addCookie(resetCookie);
        }

        request.getSession().invalidate();
        return "redirect:" + URL.LOGIN;
    }

    private void putAuthValueInSession(HttpServletRequest request) {
        request.getSession()
                .setAttribute(AuthFilter.AUTH_ATTRIBUTE,
                        cookieManager.getAuthCookieValue(request.getCookies()));
    }
}
