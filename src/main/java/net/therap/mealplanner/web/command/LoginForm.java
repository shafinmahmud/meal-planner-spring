package net.therap.mealplanner.web.command;

import javax.validation.constraints.NotNull;

/**
 * @author SHAFIN
 * @since 12/13/2016
 */
public class LoginForm {

    @NotNull
    private String userName;

    @NotNull
    private String password;

    private boolean rememberMe;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }
}
