package net.therap.mealplanner.web.command;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @author shafin
 * @since 11/30/16
 */
public class MealInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String HOUR_MINUTE_REGEX = "([0-9]{1,2}):([0-9]{1,2})";

    private long id;

    @NotNull
    private String day;

    @Size(min = 3, max = 12)
    private String type;

    @Pattern(regexp = HOUR_MINUTE_REGEX)
    private String hour;

    @NotNull
    private List<String> items;

    public MealInfo() {
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "MealDto{" +
                "day='" + day + '\'' +
                ", type='" + type + '\'' +
                ", hour='" + hour + '\'' +
                ", items='" + items + '\'' +
                '}';
    }
}
