package net.therap.mealplanner.service;

import net.therap.mealplanner.domain.LoginInfo;
import org.springframework.stereotype.Service;

/**
 * @author shafin
 * @since 7/3/18
 */
@Service
public class LoginInfoService extends AbstractService<LoginInfo> {

    public boolean isExistsUserName(String userName) {
        return findOneByField("userName", userName) != null;
    }
}
