package net.therap.mealplanner.service;

import net.therap.mealplanner.domain.Department;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author shafin
 * @since 7/3/18
 */
@Service
public class DepartmentService extends AbstractService<Department> {

    @PersistenceContext
    private EntityManager em;

    public Department findByName(String name) {
        List<Department> list = em.createQuery("FROM Department WHERE name = :name", Department.class)
                .setParameter("name", name)
                .getResultList();

        return list != null && !list.isEmpty() ? list.get(0) : null;
    }
}
