package net.therap.mealplanner.service;

import net.therap.mealplanner.domain.Department;
import net.therap.mealplanner.domain.Employee;
import net.therap.mealplanner.domain.LoginInfo;
import net.therap.mealplanner.util.CryptoUtil;
import net.therap.mealplanner.web.command.SignUpForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author shafin
 * @since 11/27/16
 */
@Service
public class EmployeeService extends AbstractService<Employee> {

    @Autowired
    private DepartmentService departmentService;

    public boolean isExistsUserEmail(String email) {
        return findOneByField("email", email) != null;
    }

    @Transactional
    public void signupEmployee(SignUpForm signUpForm) {
        Employee employee = new Employee();
        employee.setName(signUpForm.getFirstName() + " " + signUpForm.getLastName());
        employee.setEmail(signUpForm.getEmail());
        employee.setDesignation(signUpForm.getDesignation());

        Department department = departmentService.findByName(signUpForm.getDepartment());
        if (department == null) {
            department = new Department(signUpForm.getDepartment());
            em.persist(department);
        }

        employee.setDepartment(department);

        LoginInfo loginInfo = new LoginInfo(signUpForm.getUserName(),
                CryptoUtil.generateSecureHash(signUpForm.getPassword()));

        employee.setLoginInfo(loginInfo);
        em.persist(employee);
    }
}