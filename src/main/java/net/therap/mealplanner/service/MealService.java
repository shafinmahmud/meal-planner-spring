package net.therap.mealplanner.service;

import net.therap.mealplanner.domain.Day;
import net.therap.mealplanner.domain.Item;
import net.therap.mealplanner.domain.Meal;
import net.therap.mealplanner.domain.MealType;
import net.therap.mealplanner.util.DateTimeUtil;
import net.therap.mealplanner.util.RegexUtil;
import net.therap.mealplanner.web.command.MealInfo;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author shafin
 * @since 11/13/2016
 */
@Service
public class MealService extends AbstractService<Meal> {

    @Autowired
    private MealTypeService mealTypeService;

    @Autowired
    private ItemService itemService;

    public MealInfo getMealInfo(long id) {
        return convertMealToMealInfo(findOne(id));
    }

    public List<MealInfo> getAllMealInfo() {
        return findAll().stream()
                .map(this::convertMealToMealInfo)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteMeal(int id) {
        em.remove(em.getReference(Meal.class, id));
    }

    @Transactional
    public void updateMeal(MealInfo mealInfo) {
        Day day = Meal.resolveDayType(mealInfo.getDay());

        int hour = Integer.valueOf(RegexUtil.getAnyMatched(mealInfo.getHour(), MealInfo.HOUR_MINUTE_REGEX, 1));
        int minute = Integer.valueOf(RegexUtil.getAnyMatched(mealInfo.getHour(), MealInfo.HOUR_MINUTE_REGEX, 2));
        Date hourDate = DateTimeUtil.getDateFromHour(hour, minute);

        Meal meal = em.find(Meal.class, mealInfo.getId());

        if (meal == null) {
            meal = new Meal();
        }

        MealType mealType = mealTypeService.findByTypeHour(mealInfo.getType(), hourDate);

        if (mealType == null) {
            mealType = new MealType();
        }

        mealType.setHour(hourDate);
        mealType.setName(mealInfo.getType());
        em.merge(mealType);

        meal.setDay(day);
        meal.setType(mealType);

        Set<Item> items = new HashSet<>();
        for (String name: mealInfo.getItems()) {

            name = WordUtils.capitalizeFully(name.trim());
            Item item = itemService.findOneByField("name", name);
            if (item == null) {
                item = new Item(name);
                em.merge(item);
            }

            items.add(item);
        }

        meal.setItems(items);
        em.persist(meal);
    }

    private MealInfo convertMealToMealInfo(Meal meal) {
        if(meal == null) {
            return null;
        }

        MealInfo mealInfo = new MealInfo();
        mealInfo.setId(meal.getId());
        mealInfo.setDay(Meal.resolveDayType(meal.getDay()));
        mealInfo.setType(meal.getType().getName());
        mealInfo.setHour(DateTimeUtil.getHourFromDate(meal.getType().getHour()));

        Set<Item> items = meal.getItems();
        mealInfo.setItems(Item.ItemlistToStringList(items));

        return mealInfo;
    }
}
