package net.therap.mealplanner.service;

import net.therap.mealplanner.domain.Item;
import net.therap.mealplanner.domain.Meal;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shafin
 * @since 11/14/16
 */
@Service
public class ItemService extends AbstractService<Item> {

    public List<Item> getItems() {
        return findAll();
    }

    public String findItemName(long id) {
        String itemName = findOne(id).getName();
        return itemName != null ? itemName : "";
    }

    public List<String> getItemNames() {
        return getItems().stream()
                .map(Item::getName)
                .collect(Collectors.toList());
    }

    @Transactional
    public void insertItem(String itemName) {
        itemName = itemName.trim();
        if (!itemName.isEmpty() &&
                findOneByField("name", itemName) == null) {

            Item item = new Item(WordUtils.capitalizeFully(itemName));
            em.persist(item);
        }
    }

    @Transactional
    public void updateItem(long id, String itemName) {
        if (itemName != null && !itemName.isEmpty()) {
            Item oldItem = em.getReference(Item.class, id);
            if (oldItem != null) {
                oldItem.setName(WordUtils.capitalizeFully(itemName.trim()));
                em.merge(oldItem);
            }
        }
    }

    @Transactional
    public void deleteItem(int id) {
        Item item = em.getReference(Item.class, id);
        for (Meal meal : item.getMeals()) {
            meal.getItems().remove(item);
            em.merge(meal);
        }

        em.remove(item);
    }
}