package net.therap.mealplanner.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
@SuppressWarnings("unchecked")
public abstract class AbstractService<T extends Serializable> {

    private Class<T> clazz;

    @PersistenceContext
    public EntityManager em;

    public AbstractService() {
        clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    public T findOne(long id) {
        return em.find(clazz, id);
    }

    public T findOneByField(String fieldName, Object value) {
        List<T> list = em.createQuery("FROM " + clazz.getName() + " WHERE " + fieldName + " = :val", clazz)
                .setParameter("val", value)
                .getResultList();

        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    public List<T> findAll() {
        return em.createQuery("FROM " + clazz.getName())
                .getResultList();
    }

    public List<T> findAllBy(String fieldName, Object value) {
        return em.createQuery("FROM " + clazz.getName() + " WHERE " + fieldName + " = :val", clazz)
                .setParameter("val", value)
                .getResultList();
    }

    public void deleteById(long entityId) {
        T entity = findOne(entityId);
        em.remove(entity);
    }
}
