package net.therap.mealplanner.service;

import net.therap.mealplanner.domain.AuthToken;
import net.therap.mealplanner.domain.LoginInfo;
import net.therap.mealplanner.util.CryptoUtil;
import net.therap.mealplanner.web.command.LoginForm;
import net.therap.mealplanner.web.helper.CookieManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * @author shafin
 * @since 12/14/16
 */
@Service
public class AuthorizationService extends AbstractService<AuthToken> {

    @Autowired
    private LoginInfoService loginInfoService;

    public boolean isAuthorized(LoginForm loginForm) {
        LoginInfo loginInfo = loginInfoService.findOneByField("userName", loginForm.getUserName());

        return loginInfo != null && CryptoUtil.matchWithSecureHash(loginForm.getPassword(),
                loginInfo.getPassword());
    }

    public boolean isAuthTokenValid(String token) {
        if (token == null || token.isEmpty()) {
            return false;
        }

        List<AuthToken> list = em.createQuery("FROM AuthToken WHERE token = :token"
                + " and expiresOn > :today", AuthToken.class)
                .setParameter("token", token)
                .setParameter("today", new Date())
                .getResultList();

        return list != null && !list.isEmpty();
    }

    @Transactional
    public String insertUserAuth(LoginForm loginForm) throws IllegalArgumentException {
        LoginInfo loginInfo = loginInfoService.findOneByField("userName", loginForm.getUserName());
        if (loginInfo == null) {
            throw new IllegalArgumentException("Invalid login info.");
        }

        String token = CryptoUtil.generateSecureHash(loginInfo.getUserName());

        AuthToken authToken = new AuthToken();
        authToken.setLoginInfo(loginInfo);
        authToken.setToken(token);
        authToken.setExpiresOn(new Date(new Date().getTime() + CookieManager.COOKIE_AGE));

        em.persist(authToken);

        return token;
    }

    @Transactional
    public void removeAuthCookie(String token) {
        if (token != null && !token.isEmpty()) {
            findAllBy("name", token)
                    .forEach(authToken -> em.remove(authToken));
        }
    }
}
