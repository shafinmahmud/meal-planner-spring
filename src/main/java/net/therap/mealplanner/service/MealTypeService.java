package net.therap.mealplanner.service;

import net.therap.mealplanner.domain.MealType;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
@Service
public class MealTypeService extends AbstractService<MealType> {

    public MealType findByTypeHour(String type, Date hour) {
        List<MealType> list = em.createQuery("FROM MealType WHERE name = :type and hour = :hour", MealType.class)
                .setParameter("type", type)
                .setParameter("hour", hour)
                .getResultList();

        return list != null && !list.isEmpty() ? list.get(0) : null;
    }
}
