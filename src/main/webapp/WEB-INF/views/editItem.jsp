<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <title>MealPlanner :: Edit Item</title>

    <link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/sidenav.css" />" rel="stylesheet"/>

    <link href="<c:url value="/resources/css/table.css" />" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet"/>
</head>
<body>
<%@include file="jspf/navbar-frag.jspf" %>

<br>
<br><br>
<br>
<br>

<%@ include file="jspf/sidemenu-frag.jspf" %>

<div class="container">

    <form action="${formAction}" method="post" class="form-horizontal">

        <fieldset>

            <!-- Text input-->
            <div class="form-group">
                <input name="id" type="text" style="display: none" value="<c:url value="${id}" />"/>
                <label class="col-md-4 control-label" for="name"><spring:message code="form.itemName"/></label>

                <div class="col-md-4">
                    <input name="name" id="name" type="text" placeholder="eg: Bread, Butter"
                           class="form-control input-md" required="required"
                           value="<c:url value="${name}" />"/>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>

                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary"><spring:message code="btn.submit"/></button>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>

