<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%--
  @author shafin
  @since 7/3/18 4:48 PM
--%>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>GitHub
                    <li>
                    <li><a href="#">About us</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contact & support</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>Applications
                    <li>
                    <li><a href="#">Product for Mac</a></li>
                    <li><a href="#">Product for Windows</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>Services
                    <li>
                    <li><a href="#">Web analytics</a></li>
                    <li><a href="#">Presentations</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>Documentation
                    <li>
                    <li><a href="#">Product Help</a></li>
                    <li><a href="#">Developer API</a></li>
                </ul>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-8">
                <a href="#">Terms of Service</a>
                <a href="#">Privacy</a>
                <a href="#">Security</a>
            </div>
            <div class="col-md-4">
                <p class="muted pull-right">2016 FlyLeaf. All rights reserved</p>
            </div>
        </div>
    </div>
</div>
