<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <title>MealPlanner :: Edit Meal</title>

    <link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/sidenav.css" />" rel="stylesheet"/>

    <link href="<c:url value="/resources/css/table.css" />" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet"/>

</head>
<body>
<%@include file="jspf/navbar-frag.jspf" %>

<br>
<br><br>
<br>
<br>

<%@ include file="jspf/sidemenu-frag.jspf" %>

<div class="container">

    <form:form modelAttribute="mealForm" action="${formAction}" method="post" class="form-horizontal">

        <fieldset>
            <!-- Select Basic -->
            <div class="form-group">
                <form:input path="id" type="text" style="display: none"/>
                <label class="col-md-4 control-label">Day</label>

                <div class="col-md-4">
                    <form:select path="day" class="form-control" required="required">
                        <form:option value=""><spring:message code="form.chooseDay"/></form:option>
                        <form:options items="${availableDays}"/>
                    </form:select>
                    <form:errors path="day" cssClass="error"/>
                </div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label">Meal Type</label>

                <div class="col-md-4">
                    <form:select path="type" class="form-control" required="required">
                        <form:option value=""><spring:message code="form.chooseType"/></form:option>
                        <form:options items="${availableTypes}"/>
                    </form:select>
                    <form:errors path="type" cssClass="error"/>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="hour">
                    <spring:message code="form.hour"/></label>

                <div class="col-md-4">
                    <form:input path="hour" id="hour" type="text" placeholder="eg: 09:00"
                                class="form-control input-md" required="required"/>
                    <form:errors path="hour" cssClass="error"/>
                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="select-items">Item Set</label>

                <div class="col-md-4">
                    <form:select path="items" id="select-items" class="form-control" required="required">
                        <form:option value=""><spring:message code="form.itemSet"/></form:option>
                        <form:options items="${availableItems}"/>
                    </form:select>
                    <form:errors path="items" cssClass="error"/>
                </div>

            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>

                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary"><spring:message code="btn.submit"/></button>
                </div>
            </div>
        </fieldset>
    </form:form>
</div>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>